//
//  main.m
//  Loteria Cards
//
//  Created by Noemi Quezada on 12/20/14.
//  Copyright (c) 2014 noemi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NQAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NQAppDelegate class]));
    }
}
